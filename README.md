# 一  自定义插件配置

## 自定义配置插件及引入

改变vite.config.js的plugins属性的写法，改成自定义集成，以后自己写的插件自动导入并且进行集中维护。

以前写法是直接在plugins里面导入插件，但是冗余量太大，不好集中维护

```js
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()
      ],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
```

## 现在要把插件plugins抽离出去

- 在项目的根目录下创建Vite/plugins文件夹（注意与src同级）以后的自己写的或者第三方的插件配置文件都放里面，通过index集中管理，导入到Vite.config.js里面。

![1722073145208](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722073145208.png)

- vite.config.js中引入插件：

![1722074305372](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722074305372.png)



# 二  自动引入插件：

- 传统的手动引入ref、生命周期之类的以及路由等api，太麻烦且代码冗余量大，以后整合项目代码容易因为没引入某个api导致错误。

![1722073740343](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722073740343.png)

- 现在配置插件，自动去引入这些api函数，就可以省略上面手动引入ref之类的代码了
- 下载插件

```node
npm i unplugin-auto-import -D
```

```js
// Vite/plugins/auto-import.js

import autoImport from 'unplugin-auto-import/vite'

export default function createAutoImport() {
    return autoImport({
        imports: [
            'vue',
            'vue-router',
            'pinia'
        ],
        dts: false
    })
}
```

- 在index进行集成：

```js
// Vite/plugins/index.js
// 创建项目config自带的vue
import vue from '@vitejs/plugin-vue'
// 使用css框架windicss
import windCss from "vite-plugin-windicss"
// 按需引入elementplus组件库
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// 以后自己写的插件js文件引进来push到下面数组vitePlugins当中
import createAutoImport from "./auto-import"

export default function createSvgIconsPlugin() {
  const vitePlugins = [
    vue(),
    windCss(),
    AutoImport({
      resolvers: [ElementPlusResolver()
      ],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ]
  // 将刚才自定义配置的插件js文件放进去
  vitePlugins.push(createAutoImport())
  
  return vitePlugins
}
```

# 三  自定义封装SvgIcon组件，利用插件寻找Svg保存路径

- 创建文件：src/assets/icons/svg

![1722074819884](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722074819884.png)

- 使用插件帮我们指定寻找svg保存的路径
- 下载插件

```
npm install vite-plugin-svg-icons -D
```

- 在main.js引入插件

![1722075851996](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722075851996.png)

##### 补充：-D表示该依赖添加在package.json里面的devDependencies。

##### devDependencies下的依赖包，只是我们在本地或开发坏境下运行代码所依赖的，若发到线上，其实就不需要devDependencies下的所有依赖包；(比如各种loader，babel全家桶及各种webpack的插件等)只用于开发环境，不用于生产环境，因此不需要打包。可以节省优化打包体积。

- 在根目录的Vite/plugins文件夹下创建svg-icon.js文件

![1722075038863](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722075038863.png)

- ```js
  import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
  import path from 'path'
  // 指定去保存svg的文件夹中寻找对应的svg图标
  export default function createSvgIcon(isBuild) {
      return createSvgIconsPlugin({
          iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg')],
          symbolId: 'icon-[dir]-[name]',
          svgoOptions: isBuild
      })
  }
  ```

- 在index中集成插件

```js
// Vite/plugins/index.js
import vue from '@vitejs/plugin-vue'
// 使用css框架windicss
import windCss from "vite-plugin-windicss"
// 按需引入elementplus组件库
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// 以后自己写的插件js文件引进来push到下面数组vitePlugins当中
import createSvgIcon from "./svg-icon"
import createAutoImport from "./auto-import"

export default function createSvgIconsPlugin() {
  const vitePlugins = [
    vue(),
    windCss(),
    AutoImport({
      resolvers: [ElementPlusResolver()
      ],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ]

  vitePlugins.push(createSvgIcon())
  vitePlugins.push(createAutoImport())
  
  return vitePlugins
}
```

- 自动寻找Svg路径配置好了，开始封装SvgIcon显示的组件
- 创建文件：src/components/SvgIcon/index.vue

```vue
src/components/SvgIcon/index.vue

<template>
  <svg :class="svgClass" aria-hidden="true"  >
    <use :xlink:href="iconName" :fill="color" />
  </svg>
</template>

<script>
import { defineComponent, computed } from "vue";
export default defineComponent({
  props: {
    iconClass: {
      type: String,
      required: true,
    },
    className: {
      type: String,
      default: "",
    },
    color: {
      type: String,
      default: "",
    },
  },
  setup(props) {
    return {
      iconName: computed(() => `#icon-${props.iconClass}`),
      svgClass: computed(() => {
        if (props.className) {
          return `svg-icon ${props.className}`;
        }
        return "svg-icon";
      }),
    };
  },
});
</script>

<style scope lang="scss">
.sub-el-icon,
.nav-icon {
  display: inline-block;
  font-size: 15px;
  margin-right: 12px;
  position: relative;
}

.svg-icon {
  width: 1em;
  height: 1em;
  position: relative;
  fill: currentColor;
  vertical-align: -2px;
}
</style>

```

- 将封装好的SvgIcon组件引入到main.js全局共给其他组件使用

```
main.js添加
import svgicon from './components/SvgIcon'
```

- 使用例子：在src/assets/icon/svg路径下创建svg文件，将找好的svg代码放进去

![1722076124744](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722076124744.png)

![1722076254540](D:\vsPro\swc-workspaces\Vue\weichen_vite_vue\assets\1722076254540.png)

