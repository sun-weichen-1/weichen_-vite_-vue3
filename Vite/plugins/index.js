import vue from '@vitejs/plugin-vue'
// 使用css框架windicss
import windCss from "vite-plugin-windicss"
// 按需引入elementplus组件库
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// 以后自己写的插件js文件引进来push到下面数组vitePlugins当中
import createSvgIcon from "./svg-icon"
import createAutoImport from "./auto-import"

export default function createSvgIconsPlugin() {
  const vitePlugins = [
    vue(),
    windCss(),
    AutoImport({
      resolvers: [ElementPlusResolver()
      ],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ]

  vitePlugins.push(createSvgIcon())
  vitePlugins.push(createAutoImport())

  return vitePlugins
}