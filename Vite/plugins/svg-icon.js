import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import path from 'path'
// 指定去保存svg的文件夹中寻找对应的svg图标
export default function createSvgIcon(isBuild) {
    return createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg')],
        symbolId: 'icon-[dir]-[name]',
        svgoOptions: isBuild
    })
}