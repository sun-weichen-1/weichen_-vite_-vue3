import { ElNotification } from "element-plus"

export function toast(message, type = "success") {
  return ElNotification({
    message,
    type,
    duration: 2000
  })
}

export function showModal(content = "确认内容", type = "error", title = "") {
  return ElMessageBox.confirm(
    content,
    title,
    {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type,
    }
  )
}