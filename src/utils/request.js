import axios from "axios"
import { getToken } from "@/utils/auth";
import useUserStore from "@/store/modules/user";
import { toast } from "@/utils/modal";

const service = axios.create({
  baseURL: "/api",
  timeout: 10000
})

service.interceptors.request.use(function (config) {
  const token = getToken("Admin-Token")
  // 往header请求头中自动添加token
  if (token) { config.headers["token"] = token }
  return config;
}, function (error) {
  return Promise.reject(error);
});

service.interceptors.response.use(
  function (response) {
    return response.data.data;
  },
  function (error) {
    const userStore = useUserStore();
    const msg = error.response.data.msg || "请求失败"
    if (msg == "你运行到这一步，代表已经修改成功了，但由于当前账号是课程演示账号，所以不会真实修改~") {
      userStore.logout().finally(() => location.reload())
    }
    toast(msg, "error")
    return Promise.reject(error);
  });

export default service