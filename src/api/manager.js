import request from "@/utils/request"

export function loginAPI(data = { username, password }) {
  return request({
    url: "/admin/login",
    method: 'post',
    data: data
  })
}

export function getInfoAPI() {
  return request({
    url: "/admin/getinfo",
    method: 'post',
  })
}

export function logoutAPI() {
  return request({
    url: "/admin/logout",
    method: 'post',
  })
}


export function updatePassAPI(data) {
  return request({
    url: "/admin/updatepassword",
    method: 'post',
    data
  })
}