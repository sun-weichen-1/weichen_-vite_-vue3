import router from "@/router"
import NProgress from 'nprogress'
import { toast } from "../utils/modal"
import { getToken } from "@/utils/auth"
import useUserStore from "@/store/modules/user";

router.beforeEach(async (to, from, next) => {
  NProgress.start()
  const token = getToken()
  // not has token
  if (!token && to.path !== '/login') {
    toast("请先登录！", 'error')
    return next({ path: '/login' })
  }
  // has token
  if (token && to.path === '/login') {
    toast("请勿重复登录！", 'error')
    return next({ path: from.path ? from.path : '/' })
  }
  // if login success auto getuserinfo and to set store's state
  const userStore = useUserStore();
  if (token) {
    await userStore.getinfo()
  }
  // System title
  let title = (to.meta.title ? to.meta.title : "") + "-伟晨编程"
  document.title = title
  next()
})
// close nprogress
router.afterEach(() => {
  NProgress.done()
})