import { createRouter, createWebHashHistory } from "vue-router"
import Index from "@/pages/home/index.vue"
import AdminPages from "@/layout/admin.vue"
const routes = [
  {
    path: "/",
    component: AdminPages,
    children: {
      path: "/",
      component: Index,
      meta: {
        title: "系统首页"
      }
    }
  },
  {
    path: "/login",
    component: () => import("@/pages/login.vue"),
    meta: {
      title: "登录"
    }
  },
  {
    path: "/:pathMatch(.*)",
    component: () => import("@/pages/error/404.vue"),
    meta: {
      title: "404错误"
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router