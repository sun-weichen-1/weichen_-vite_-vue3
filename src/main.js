import { createApp } from 'vue'

import './style.css'
import "normalize.css"
import 'element-plus/dist/index.css'

import App from './App'
import store from "./store"
import router from './router'

// use myself plugins 
import plugin from "@/plugins/index"

// use svgIcon
import 'virtual:svg-icons-register'
import svgicon from './components/SvgIcon'
import elementIcons from '@/components/SvgIcon/svgicon'

import '@/router/permission'

import 'nprogress/nprogress.css'

const app = createApp(App)

app.use(store)
app.use(plugin)
app.use(router)
app.use(elementIcons)
app.component('svg-icon', svgicon)

app.mount('#app')
