import { setToken, removeToken } from "@/utils/auth"
import { loginAPI, getInfoAPI, logoutAPI } from "@/api/manager"

const useUserStore = defineStore(
  'user',
  {
    state: () => (
      {
        user: {}
      }
    ),
    actions: {
      login(loginForm) {
        return new Promise((resolve, reject) => {
          loginAPI(loginForm)
            .then((res) => {
              setToken(res.token);
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
        })
      },

      getinfo() {
        return new Promise((resolve, reject) => {
          getInfoAPI()
            .then((res) => {
              this.user = res
              resolve(res)
            })
            .catch(err => {
              reject(err)
            });
        })
      },

      logout() {
        return new Promise((resolve, reject) => {
          logoutAPI()
            .then((res) => {
              removeToken()
              this.user = {}
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
        })
      },
    }
  }
)

export default useUserStore