import { defineConfig } from 'vite'
import createSvgIconsPlugin from "./Vite/plugins"
import path from "path"
export default defineConfig({
  plugins: [
    createSvgIconsPlugin()
  ],
  server: {
    proxy: {
      '/api': {
        target: 'http://ceshi13.dishait.cn',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    },
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
  },
})
